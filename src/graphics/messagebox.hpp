#ifndef _H_MESSAGEBOX_
#define _H_MESSAGEBOX_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>

namespace graphics{
  class MessageBox{
  public:
    MessageBox(const char* filename, SDL_Rect src, SDL_Rect dst);
    void update(std::string line1str, std::string line2str);
    void render();
    ~MessageBox();
  private:
    bool isIn;
    SDL_Texture* objTexture;
    SDL_Texture* line1Texture;
    SDL_Texture* line2Texture;
    SDL_Rect srcRectObj,destRectObj;
    SDL_Rect srcRectline1,destRectline1;
    SDL_Rect srcRectline2,destRectline2;
  };
}

#endif
