#include "gameoptions.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace graphics;
using namespace std;

GameOptions::GameOptions(){
  this->numberOfPlayers = 0;
  this->mapLength = 0;
  this->playerNames.clear();
}

void GameOptions::setNumberOfPlayers(){
  std::cout << "Hey, your game is about to start, " << '\n';
  std::cout << "Insert the number of players between 1 and 8" << '\n';
  string s;
  std::cin >> s;
  while((int)s.at(0)-48 < 1 || (int)s.at(0)-48 > 8 || s.length() > 1){
    std::cout << "Sorry your answer is not valid, try to insert a number between 1 and 8" << '\n';
    std::cin >> s;
  }
  this->numberOfPlayers = (int)s.at(0)-48;
  std::cout << '\n';
}

int GameOptions::getNumberOfPlayers(){
  return this->numberOfPlayers;
}
void GameOptions::setMapLength(){
  std::cout << "Now you just have to insert the length of the map that you want to play on" << '\n';
  std::cout << "1) Short Map (40-80)" << '\n';
  std::cout << "2) Medium Map (80-140)" << '\n';
  std::cout << "3) Long Map (140-250)" << '\n';
  std::cout << "4) Very Long Map (250-400)" << '\n';
  char s;
  std::cin >> s;
  while((int)s-48 < 1 || (int)s-48 > 4){
    std::cout << "Sorry your answer is not valid, try to insert a number between 1 and 4" << '\n';
    std::cin >> s;
  }
  this->mapLength = (int)s-48;
  std::cout << '\n';
}
int GameOptions::getMapLength(){
  return this->mapLength;
}
void GameOptions::setPlayerNames(int n){
  this->playerNames.clear();
  std::string s;
  for (int i = 0; i < this->numberOfPlayers; i++) {
    std::cout << "Insert the name of Player " << i + 1 << ": (names under 10 character are recommended)" << '\n';
    std::cin >> s;
    this->playerNames.push_back(s);
  }
    std::cout << '\n';
}
std::vector<string> GameOptions::getPlayerNames(){
  return this->playerNames;
}
GameOptions::~GameOptions(){
}
