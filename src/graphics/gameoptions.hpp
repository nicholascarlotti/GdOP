#ifndef _H_GAMEOPTIONS_
#define _H_GAMEOPTIONS_
#include <string>
#include <vector>

namespace graphics{
  class GameOptions{
  public:
    GameOptions();
    void setNumberOfPlayers();
    int getNumberOfPlayers();
    void setMapLength();
    int getMapLength();
    void setPlayerNames(int n);
    std::vector<std::string> getPlayerNames();
    ~GameOptions();
  private:
    std::vector<std::string> playerNames;
    int mapLength;
    int numberOfPlayers;
  };
}

#endif
