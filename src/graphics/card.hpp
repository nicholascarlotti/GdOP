#ifndef _H_GCARD_
#define _H_GCARD_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "button.hpp"
namespace graphics{
  class GCard{
  public:
    GCard(const char *bg, SDL_Rect srcRect, SDL_Rect destRect);
    void update(std::string s1, std::string s2);
    void render();
    
    Button* getEffect1Button();
    Button* getEffect2Button();
    ~GCard();
  private:
    SDL_Texture* objTexture;
    Button* effect1Button;
    Button* effect2Button;
    SDL_Rect srcRectObj,destRectObj;
  };
}

#endif
