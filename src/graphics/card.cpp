#include "./card.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "button.hpp"
using namespace graphics;

GCard::GCard(const char* bg, SDL_Rect srcRect, SDL_Rect destRect){
  srcRectObj = srcRect;
  destRectObj = destRect;
  SDL_Rect effect1Dsttmp;
  effect1Dsttmp.x = destRectObj.x + (int)((destRectObj.w / (double)srcRectObj.w) * 15);
  effect1Dsttmp.y = destRectObj.y + (int)((destRectObj.h / (double)srcRectObj.h) * 15);
  effect1Dsttmp.w = (int)((destRectObj.w / (double)srcRectObj.w) * 370);
  effect1Dsttmp.h = (int)((destRectObj.h / (double)srcRectObj.h) * 305);
  SDL_Rect effect2Dsttmp;
  effect2Dsttmp.x = destRectObj.x + (int)((destRectObj.w / (double)srcRectObj.w) * 15);
  effect2Dsttmp.y = destRectObj.y + (int)((destRectObj.h / (double)srcRectObj.h) * 320);
  effect2Dsttmp.w = (int)((destRectObj.w / (double)srcRectObj.w) * 370);
  effect2Dsttmp.h = (int)((destRectObj.h / (double)srcRectObj.h) * 305);
  objTexture = TextureManager::loadTexture(bg);
  this->effect1Button = new Button("assets/topchoicecard.png", "", {0,0,370,305}, effect1Dsttmp,{200,200,200,255});
  this->effect2Button = new Button("assets/bottomchoicecard.png", "", {0,0,370,305}, effect2Dsttmp,{200,200,200,255});
}

void GCard::update(std::string s1, std::string s2){
  effect1Button->update(s1);
  effect2Button->update(s2);
}

void GCard::render(){
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
  this->effect1Button->render();
  this->effect2Button->render();
}
Button* GCard::getEffect1Button(){
  return this->effect1Button;
}
Button* GCard::getEffect2Button(){
  return this->effect2Button;
}
GCard::~GCard(){
  SDL_DestroyTexture(objTexture);
}
