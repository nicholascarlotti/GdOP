#include "background.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

using namespace graphics;

Background::Background(const char* bg){
  objTexture = TextureManager::loadTexture(bg);
}

void Background::update(SDL_Rect camera) {
  SDL_Rect objRect;
  SDL_QueryTexture(objTexture, NULL, NULL, &objRect.w, &objRect.h);
  int scalew = camera.w / 1280;
  int scaleh = camera.h / 720;
  srcRectObj.x = camera.x / 47;
  srcRectObj.y = camera.y;
  srcRectObj.h = 600;
  srcRectObj.w = camera.w;
  destRectObj.x = 0;
  destRectObj.y = 0;
  destRectObj.w = camera.w;
  destRectObj.h = 440;
}

void Background::render(){
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
}
Background::~Background(){
  SDL_DestroyTexture(objTexture);
}
