#include "messagebox.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>

using namespace graphics;

MessageBox::MessageBox(const char* filename, SDL_Rect src, SDL_Rect dst){
  objTexture = TextureManager::loadTexture(filename);
  srcRectObj = src;
  destRectObj = dst;
}

void MessageBox::update(std::string line1str, std::string line2str) {
  char const* line1 = line1str.c_str();
  line1Texture = TextureManager::createTextureFromTextLight(line1, {255,255,255,255});
  SDL_QueryTexture(line1Texture, NULL, NULL, &srcRectline1.w, &srcRectline1.h);
  srcRectline1.x = 0;
  srcRectline1.y = 0;
  destRectline1.x = destRectObj.x + 10;
  destRectline1.y = destRectObj.y + 10;
  destRectline1.h = srcRectline1.h;
  destRectline1.w = srcRectline1.w;
  char const* line2 = line2str.c_str();
  line2Texture = TextureManager::createTextureFromTextLight(line2, {255,255,255,255});
  SDL_QueryTexture(line2Texture, NULL, NULL, &srcRectline2.w, &srcRectline2.h);
  srcRectline2.x = 0;
  srcRectline2.y = 0;
  destRectline2.x = destRectline1.x;
  destRectline2.y = destRectline1.y + destRectline1.h + 10;
  destRectline2.h = srcRectline2.h;
  destRectline2.w = srcRectline2.w;
}

void MessageBox::render(){
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
  TextureManager::draw(this->line1Texture, srcRectline1, destRectline1);
  TextureManager::draw(this->line2Texture, srcRectline2, destRectline2);
}

MessageBox::~MessageBox(){
  SDL_DestroyTexture(this->objTexture);
  SDL_DestroyTexture(this->line1Texture);
  SDL_DestroyTexture(this->line2Texture);
}
