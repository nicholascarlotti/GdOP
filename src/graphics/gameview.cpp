#include "gameview.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include <iostream>
#include "../gamelogic/gamestatus.hpp"
#include "../objects/map.hpp"
#include "background.hpp"
#include "button.hpp"
#include "card.hpp"
#include "dice.hpp"
#include "messagebox.hpp"
#include "playerview.hpp"
#include "texturemanager.hpp"
#include "tile.hpp"

using namespace graphics;
using namespace std;

SDL_Renderer* GameView::renderer = NULL;
TTF_Font* GameView::globalFont = NULL;
TTF_Font* GameView::messageboxFont = NULL;

GameView::GameView() {}

void GameView::init(gopgl::GameStatus* gs) {
    this->gameStatus = gs;
    this->isRunning = true;
    int flags = 0;
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        std::cout << "Subsystems initialized!" << '\n';

        window = SDL_CreateWindow("Gdop", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, 0);
        if (window) {
            std::cout << "Window created!" << '\n';
            SDL_SetWindowResizable(window, SDL_FALSE);
        }
        renderer = SDL_CreateRenderer(window, -1, 0);
        if (renderer) {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            std::cout << "Renderer Created!" << '\n';
        }
    }
    if (TTF_Init() == -1) {
        printf("TTF_Init: %s\n", TTF_GetError());
    }
    this->globalFont = TTF_OpenFont("./assets/OpenSans-ExtraBold.ttf", 28);
    if (!globalFont) {
        printf("TTF_OpenFont: %s\n", TTF_GetError());
    }
    this->messageboxFont = TTF_OpenFont("./assets/OpenSans-ExtraBold.ttf", 14);
    if (!messageboxFont) {
        printf("TTF_OpenFont: %s\n", TTF_GetError());
    }
    camera = {0, 0, 1280, 720};
    SDL_Texture* loadgame = TextureManager::loadTexture("assets/loadgame.png");
    SDL_RenderCopy(renderer, loadgame, NULL, NULL);
    SDL_RenderPresent(renderer);
    loadAssets();
    this->frameStart = SDL_GetTicks();
}
void GameView::setStage(int s) {
    this->stage = s;
}

int GameView::getStage() {
    return this->stage;
}
void GameView::setDie(int n, int val) {
    switch (n) {
        case 0:
            this->dice1->setNumber(val);
            break;
        case 1:
            this->dice2->setNumber(val);
            break;
        case 2:
        case 3:
            this->diceUpper->setNumber(val);
            this->diceLower->setNumber(val);
            break;
        default:
            this->dice1->setNumber(1);
            this->dice2->setNumber(1);
            this->diceUpper->setNumber(1);
            this->diceLower->setNumber(1);
            break;
    }
}
int GameView::getDie(int n) {
    switch (n) {
        case 0:
            return this->dice1->getNumber();
            break;
        case 1:
            return this->dice2->getNumber();
            break;
        case 2:
            return this->diceUpper->getNumber();
            break;
        case 3:
            return this->diceLower->getNumber();
            break;
        default:
            return 0;
            break;
    }
}
void GameView::loadAssets() {
    lowerBackground = new Background("assets/lowerBackground.png");
    upperBackground = new Background("assets/upperBackground.png");
    rollButton = new Button("assets/rollbutton.png", "ROLL", {0, 0, 150, 150}, {565, 550, 150, 150});
    topButton = new Button("assets/rollbutton.png", "", {0, 0, 150, 50}, {565, 550, 150, 50});
    middleButton = new Button("assets/rollbutton.png", "", {0, 50, 150, 50}, {565, 600, 150, 50});
    bottomButton = new Button("assets/rollbutton.png", "", {0, 100, 150, 50}, {565, 650, 150, 50});
    leftButton = new Button("assets/rollbutton.png", "", {0, 0, 75, 150}, {565, 550, 75, 150});
    rightButton = new Button("assets/rollbutton.png", "", {75, 0, 75, 150}, {640, 550, 75, 150});
    centerButton = new Button("assets/center.png", "", {0, 0, 50, 50}, {10, 10, 50, 50});
    minimapButton = new Button("assets/minimapbar.png", "", {0, 0, 500, 20}, {775, 5, 500, 20});
    lowerWorldButton = new Button("assets/lowerworldbutton.png", "", {0, 0, 75, 95}, {565, 625, 75, 95});
    upperWorldButton = new Button("assets/upperworldbutton.png", "", {0, 0, 75, 95}, {640, 625, 75, 95});
    messagebox = new MessageBox("assets/messagebox.png", {0, 0, 640, 125}, {0, 595, 640, 125});
    tempmessagebox = new MessageBox("assets/rightmenu.png", {0, 0, 640, 125}, {640, 595, 640, 125});
    dice1 = new Dice("assets/dice.png", 0, {770, 610, 100, 100});
    dice2 = new Dice("assets/dice.png", 0, {1110, 610, 100, 100});
    diceLower = new Dice("assets/dice.png", 2, {940, 610, 100, 100});
    diceUpper = new Dice("assets/dice.png", 1, {940, 610, 100, 100});
    for (int i = 0; i < this->gameStatus->getPlayerCount(); i++) {
        players[i] = new PlayerView("./assets/player.png", "./assets/playerpointer.png", i, this->gameStatus->getPlayerCount(), &this->gameStatus->getPlayers()->at(i), this->gameStatus->getUpperMap()->getSize());
    }

    for (int i = 0; i < this->gameStatus->getUpperMap()->getSize(); i++) {
        if (i % 2) {
            upperTiles[i] = new Tile("assets/eventileupper.png", "assets/flags.png", i, this->gameStatus->getUpperMap()->getCell(i)->getEffect());
        } else {
            upperTiles[i] = new Tile("assets/oddtileupper.png", "assets/flags.png", i, this->gameStatus->getUpperMap()->getCell(i)->getEffect());
        }
    }

    for (int i = 0; i < this->gameStatus->getLowerMap()->getSize(); i++) {
        if (i % 2) {
            lowerTiles[i] = new Tile("assets/eventilelower.png", "assets/flags.png", i, this->gameStatus->getUpperMap()->getCell(i)->getEffect());
        } else {
            lowerTiles[i] = new Tile("assets/oddtilelower.png", "assets/flags.png", i, this->gameStatus->getUpperMap()->getCell(i)->getEffect());
        }
    }
    int lastTileX = this->gameStatus->getUpperMap()->getSize() * 150;
    if (this->gameStatus->getUpperMap()->getSize() % 2) {
        upperEndTile = new Tile("assets/eventileupperendtile.png", {0, 0, 350, 200}, {lastTileX, 400, 350, 200});
        lowerEndTile = new Tile("assets/eventilelowerendtile.png", {0, 0, 350, 200}, {lastTileX, 400, 350, 200});
    } else {
        upperEndTile = new Tile("assets/oddtileupperendtile.png", {0, 0, 350, 200}, {lastTileX, 400, 350, 200});
        lowerEndTile = new Tile("assets/oddtilelowerendtile.png", {0, 0, 350, 200}, {lastTileX, 400, 350, 200});
    }

    this->pickingCard = new graphics::GCard("assets/card.png", {0, 0, 400, 640}, {540, 120, 200, 320});
    victoryScreen = new Button("assets/victoryscreen.png", "", {0, 0, 800, 300}, {240, 50, 800, 300});
}

void GameView::handleUIResponsiveness(SDL_Event event) {
    if (event.type == SDL_MOUSEBUTTONDOWN) {
        if (minimapButton->isInside(event.motion.x, event.motion.y)) {
            minimapUpdate = true;
            camera.x = ((event.motion.x - 775) / 5) * this->gameStatus->getUpperMap()->getSize() * 1.5 - 320;
            if (camera.x < 0) {
                camera.x = 0;
            }
            if (camera.x > 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350) {
                camera.x = 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350;
            }
        }

        //World visualization handling
        if (!this->rollButton->isInsideCircular(event.motion.x, event.motion.y)) {
            if (upperWorldButton->isInside(event.motion.x, event.motion.y)) {
                upperWorldFocus = true;
            }
            if (lowerWorldButton->isInside(event.motion.x, event.motion.y)) {
                upperWorldFocus = false;
            }
        }

        //Current player focus handling
        if (centerButton->isInsideCircular(event.motion.x, event.motion.y)) {
            camera.x = this->gameStatus->getCurrentPlayer()->getPosition() * 150;
            if (camera.x < 0) {
                camera.x = 0;
            }
            if (camera.x >
                150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350) {
                camera.x =
                    150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350;
            }
            upperWorldFocus = !this->gameStatus->getCurrentPlayer()->isInLowerMap();
        }

    } else if (event.type == SDL_MOUSEBUTTONUP) {
        minimapUpdate = false;

    } else if (event.type == SDL_MOUSEMOTION) {
        Tile** tiles;
        if (upperWorldFocus) {
            tiles = upperTiles;
        } else {
            tiles = lowerTiles;
        }

        for (int i = 0; i < this->gameStatus->getUpperMap()->getSize(); i++) {
            if (tiles[i]->isInsideFlag(event.motion.x, event.motion.y)) {
                flagDescription = true;
                std::string line1 = tiles[i]->getEffect()->getName();
                std::string line2 = tiles[i]->getEffect()->getDescription();
                this->printMessage(line1, line2);
                i = this->gameStatus->getUpperMap()->getSize();
            }
        }

        if (minimapButton->isInside(event.motion.x, event.motion.y)) {
            this->printMessage("Minimap", "Click anywhere on it to move through map");
        }

        // Minimap and map scroll handling
        if (minimapButton->isInside(event.motion.x, event.motion.y) && minimapUpdate) {
            camera.x = ((event.motion.x - 775) / 5) * this->gameStatus->getUpperMap()->getSize() * 1.5 - 320;
            if (camera.x < 0) {
                camera.x = 0;
            }
            if (camera.x > 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350) {
                camera.x = 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350;
            }
        }
        if (event.motion.x > 1180 && event.motion.y > 100 && event.motion.y < 600) {
            cameraSpeed = 15;
            if (event.motion.x > 1260) {
                cameraSpeed = 50;
            }
            cameraUpdate = true;
        } else if (event.motion.x < 100 && event.motion.y > 100 && event.motion.y < 600) {
            cameraSpeed = -15;
            if (event.motion.x < 20) {
                cameraSpeed = -50;
            }
            cameraUpdate = true;
        } else {
            cameraUpdate = false;
        }

        // Buttons highligt management
        bool insideRollBtn = rollButton->isInsideCircular(event.motion.x, event.motion.y);
        rollButton->highlight(insideRollBtn);
        upperWorldButton->highlight(upperWorldButton->isInside(event.motion.x, event.motion.y) && !insideRollBtn);
        lowerWorldButton->highlight(lowerWorldButton->isInside(event.motion.x, event.motion.y) && !insideRollBtn);

        // Effect die description on hover
        int effectDieIndex = this->diceUpper->getNumber() - 1;
        if (this->upperWorldFocus && this->diceUpper->isInside(event.motion.x, event.motion.y)) {
            this->printMessage("Effect dice:", this->diceEffectDescr[effectDieIndex]);
        } else if (!this->upperWorldFocus && this->diceLower->isInside(event.motion.x, event.motion.y)) {
            this->printMessage("Effect dice:", this->diceEffectDescr[effectDieIndex + 6]);
        }

        // Choice button highlight on hover
        this->topButton->highlight(this->topButton->isInside(event.motion.x, event.motion.y) && insideRollBtn);
        this->middleButton->highlight(this->middleButton->isInside(event.motion.x, event.motion.y) && insideRollBtn);
        this->bottomButton->highlight(this->bottomButton->isInside(event.motion.x, event.motion.y) && insideRollBtn);
    }
}

void GameView::cameraMove() {
    camera.x += cameraSpeed;
    if (camera.x < 0) {
        camera.x = 0;
    }
    if (camera.x > 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350) {
        camera.x = 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350;
    }
}

void GameView::update() {
    this->frameStart = SDL_GetTicks();
    SDL_GetWindowSize(window, &camera.w, &camera.h);
    if (cameraUpdate) {
        cameraMove();
    }
    upperBackground->update(camera);
    lowerBackground->update(camera);
    for (int i = 0; i < this->gameStatus->getUpperMap()->getSize(); i++) {
        upperTiles[i]->update(camera);
    }
    for (int i = 0; i < this->gameStatus->getLowerMap()->getSize(); i++) {
        lowerTiles[i]->update(camera);
    }
    if (roll) {
        if (this->gameStatus->getNextPlayer()->getPosition() > 3) {
            camera.x = ((this->gameStatus->getNextPlayer()->getPosition() - 1) * 150);
        } else {
            camera.x = 0;
        }
    }
    for (int i = 0; i < this->gameStatus->getPlayerCount(); i++) {
        players[i]->update(camera, this->gameStatus->getCurrentPlayer(), SDL_GetTicks());
    }

    dice1->update();
    dice2->update();
    diceUpper->update();
    diceLower->update();
    tempmessagebox->update("", "");
    rollButton->update();
    centerButton->update();
    minimapButton->update();
    lowerWorldButton->update();
    upperWorldButton->update();
    upperEndTile->updateLastTile(camera);
    lowerEndTile->updateLastTile(camera);
}
void GameView::render() {
    if (upperWorldFocus) {
        SDL_RenderClear(renderer);
        upperBackground->render();
        for (int i = 0; i < this->gameStatus->getUpperMap()->getSize(); i++) {
            upperTiles[i]->render();
        }
        upperEndTile->renderLastTile();
    } else {
        lowerBackground->render();
        for (int i = 0; i < this->gameStatus->getUpperMap()->getSize(); i++) {
            lowerTiles[i]->render();
        }
        lowerEndTile->renderLastTile();
    }

    minimapButton->render();
    for (int i = this->gameStatus->getPlayerCount() - 1; i >= 0; i--) {
        players[i]->render(upperWorldFocus);
    }
    if ((abs(camera.x - this->gameStatus->getCurrentPlayer()->getPosition() * 150) > 450) || upperWorldFocus == this->gameStatus->getCurrentPlayer()->isInLowerMap()) {
        centerButton->render();
    }
    messagebox->render();
    tempmessagebox->render();
    dice1->render();
    dice2->render();
    if (!this->gameStatus->getCurrentPlayer()->isInLowerMap()) {
        diceUpper->render();
    } else {
        diceLower->render();
    }

    if (rollStage) {
        rollButton->render();
    } else if (diceChoiceStage) {
        topButton->render();
        middleButton->render();
        bottomButton->render();
    }

    if (cardChoiceStage) {
        leftButton->render();
        rightButton->render();
    }

    if (this->cardStage) {
        this->pickingCard->render();
    }

    lowerWorldButton->render();
    upperWorldButton->render();
    if (this->victory) {
        victoryScreen->render();
    }
    SDL_RenderPresent(renderer);
    capFramerate(this->frameStart);
}
bool GameView::getRoll() { return roll; }
void GameView::setRoll(bool b) {
    roll = b;
}
void GameView::swipeCamera(int dest) {
    int distance = (dest - camera.x) / 50;
    swipe = false;
    for (int i = 0; i < 50; i++) {
        SDL_Delay(20);
        update();
        camera.x += distance;
    }
    swipe = true;
}

void GameView::capFramerate(Uint32 startingTick) {
    if ((SCREEN_TICK_PER_FRAME) > SDL_GetTicks() - startingTick) {
        SDL_Delay((SCREEN_TICK_PER_FRAME) - (SDL_GetTicks() - startingTick));
    }
}
bool GameView::isClosed() {
    return !this->isRunning;
}
void GameView::clean() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    TTF_Quit();

    delete lowerBackground;
    delete upperBackground;
    delete rollButton;
    delete topButton;
    delete middleButton;
    delete bottomButton;
    delete leftButton;
    delete rightButton;
    delete centerButton;
    delete minimapButton;
    delete lowerWorldButton;
    delete upperWorldButton;
    delete messagebox;
    delete tempmessagebox;

    std::cout << "Game Cleaned!" << '\n';
}
GameView::~GameView() {
}

int GameView::messageDialog(string message) {
    int result = 0;
    bool captured = false;
    SDL_Event event;
    cardChoiceStage = false;
    diceChoiceStage = false;
    rollStage = true;

    while (!captured) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_MOUSEBUTTONUP) {
                if (rollButton->isInsideCircular(event.motion.x, event.motion.y)) {
                    captured = true;
                }
            } else if (event.type == SDL_QUIT) {
                isRunning = false;
                captured = true;
                result = -1;
            }
            this->handleUIResponsiveness(event);
        }

        this->update();
        rollButton->update(message);
        this->render();
    }
    return result;
}

void GameView::setRollStage() {
    this->rollStage = true;
    this->cardChoiceStage = false;
    this->diceChoiceStage = false;
    this->update();
    this->render();
}

void GameView::printMessage(string m1, string m2) {
    this->messagebox->update(m1, m2);
    this->messagebox->render();
}

int GameView::renderNewTurnStage(string playerName) {
    int result = 0;
    bool captured = false;
    SDL_Event event;
    this->setRollStage();
    this->printingMessage = true;
    this->printMessage("It's your turn: ", playerName);
    rollButton->update("Start");
    setCameraX((this->gameStatus->getCurrentPlayer()->getPosition() - 1) * 150);
    while (!captured) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_MOUSEBUTTONUP) {
                if (rollButton->isInsideCircular(event.motion.x, event.motion.y)) {
                    captured = true;
                }
            } else if (event.type == SDL_QUIT) {
                isRunning = false;
                captured = true;
                result = -1;
            }
            this->handleUIResponsiveness(event);
        }

        this->update();
        this->render();
    }
    return result;
}

int GameView::cardEffectPickingDialog(gopo::Card* card) {
    int result = 0;
    bool captured = false;
    SDL_Event event;
    this->pickingCard->update(card->getEffect1().getName(), card->getEffect2().getName());
    this->setCardStage();
    while (!captured) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_MOUSEBUTTONUP) {
                if (this->pickingCard->getEffect1Button()->isInside(event.motion.x, event.motion.y)) {
                    result = 0;
                    captured = true;
                } else if (this->pickingCard->getEffect2Button()->isInside(event.motion.x, event.motion.y)) {
                    result = 1;
                    captured = true;
                }
            } else if (event.type == SDL_QUIT) {
                isRunning = false;
                captured = true;
                result = -1;
            }else if(event.type == SDL_MOUSEMOTION){
                if (this->pickingCard->getEffect1Button()->isInside(event.motion.x, event.motion.y)) {
                    this->printMessage("Card effect:", card->getEffect1().getDescription());
                } else if (this->pickingCard->getEffect2Button()->isInside(event.motion.x, event.motion.y)) {
                    this->printMessage("Card effect:", card->getEffect2().getDescription());
                }
            }
            this->handleUIResponsiveness(event);
        }

        this->update();
        this->render();
    }

    this->unsetCardStage();
    return result;
}

void GameView::setCardStage() {
    this->cardStage = true;
}

void GameView::unsetCardStage() {
    this->cardStage = false;
}
int GameView::specialDiceChoiceDialog() {
    int result = 0;
    bool captured = false;
    SDL_Event event;
    topButton->update(this->specialDiceChoiceText[0]);
    middleButton->update(this->specialDiceChoiceText[1]);
    bottomButton->update(this->specialDiceChoiceText[2]);
    this->rollStage = false;
    this->diceChoiceStage = true;

    while (!captured) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_MOUSEBUTTONUP) {
                if (this->topButton->isInside(event.motion.x, event.motion.y)) {
                    captured = true;
                    result = 0;
                } else if (this->middleButton->isInside(event.motion.x, event.motion.y)) {
                    captured = true;
                    result = 1;
                } else if (this->bottomButton->isInside(event.motion.x, event.motion.y)) {
                    captured = true;
                    result = 2;
                }
            } else if (event.type == SDL_QUIT) {
                captured = true;
                isRunning = false;
                result = -1;
            }

            this->handleUIResponsiveness(event);
        }
        this->update();
        this->render();
    }
    return result;
}

void GameView::setCameraX(int x) {
    this->camera.x = x;
    if (camera.x < 0) {
        camera.x = 0;
    }
    if (camera.x > 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350) {
        camera.x = 150 * this->gameStatus->getUpperMap()->getSize() - 1280 + 350;
    }
    upperWorldFocus = !this->gameStatus->getCurrentPlayer()->isInLowerMap();
}

void GameView::setVictory() {
    this->victory = true;
}
