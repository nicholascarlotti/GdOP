#ifndef _H_DICE_
#define _H_DICE_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "button.hpp"
#include "sprite.hpp"

namespace graphics{
  class Dice : Sprite{
  public:
    Dice(const char* bg, int t, SDL_Rect destRect);
    void update();
    void render();
    bool isInside(int, int);
    bool isInsideCircular(int, int);
    void setNumber(int);
    int getNumber();
    ~Dice();
  private:
    int number;
    int type;
    SDL_Texture* objTexture;
    SDL_Rect srcRectObj,destRectObj;
  };
}

#endif
