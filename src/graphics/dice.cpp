#include "dice.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "sprite.hpp"

using namespace graphics;

Dice::Dice(const char* bg, int t, SDL_Rect destRect) : Sprite(bg){
  this->number = 1;
  this->type = t;
  destRectObj = destRect;
  srcRectObj = {0,0,200,200};
  objTexture = TextureManager::loadTexture(bg);
}

void Dice::update() {
  srcRectObj.x = (number-1) * 200;
  srcRectObj.y = type * 200;
  srcRectObj.w = 200;
  srcRectObj.h = 200;
}

void Dice::render(){
  SDL_SetTextureAlphaMod(this->objTexture, 200);
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
}
void Dice::setNumber(int n){
  this->number = n;
}
int Dice::getNumber(){
  return this->number;
}
Dice::~Dice(){
  SDL_DestroyTexture(objTexture);
}

bool Dice::isInside(int x, int y){
  return x > destRectObj.x && x < (destRectObj.w + destRectObj.x) && y > destRectObj.y && y < (destRectObj.y + destRectObj.h);
}

bool Dice::isInsideCircular(int x, int y){
  int cx = destRectObj.x + (destRectObj.w / 2);
  int cy = destRectObj.y + (destRectObj.h / 2);
  return sqrt(((cx-x)*(cx-x))+((cy-y)*(cy-y))) < ((destRectObj.w / 2)) && sqrt(((cx-x)*(cx-x))+((cy-y)*(cy-y))) < (destRectObj.h / 2);
}
