#ifndef _H_PLAYERVIEW_
#define _H_PLAYERVIEW_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "../objects/player.hpp"

namespace graphics{
  class PlayerView{
  public:
    PlayerView(const char* bg, const char* minimapPlayer,int playerN,int totPlayers, gopo::Player* player, int mapLength);
    void update(SDL_Rect camera,gopo::Player* currentPlayer ,Uint32 tick);
    void render(bool worldFocus);
    ~PlayerView();
  private:
    int Rmod,Gmod,Bmod;
    gopo:: Player* p;
    SDL_Texture* objTexture;
    SDL_Texture* minimapTexture;
    SDL_Texture* playerNameTexture;
    int mapL;
    int playerCount;
    int playerNumber;
    SDL_Rect srcRectObj,srcRectPlayerName,destRectObj,destRectPlayerName, srcRectMP, destRectMP;
  };
}

#endif
