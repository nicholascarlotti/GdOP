#include "button.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>

using namespace graphics;

Button::Button(const char* filename,const char* text, SDL_Rect src, SDL_Rect dst) : Sprite(filename){
  objTexture = TextureManager::loadTexture(filename);
  this->textColor = {0,0,0,255};
  textTexture = TextureManager::createTextureFromText(text,"./assets/OpenSans-ExtraBold.ttf",28,this->textColor);
  srcRectObj = src;
  destRectObj = dst;
  isInside(0,0);
  this->isIn = false;
}
Button::Button(const char* filename,const char* text, SDL_Rect src, SDL_Rect dst,SDL_Color tC) : Sprite(filename){
  objTexture = TextureManager::loadTexture(filename);
  this->textColor = tC;
  textTexture = TextureManager::createTextureFromText(text,"./assets/OpenSans-ExtraBold.ttf",28,this->textColor);
  srcRectObj = src;
  destRectObj = dst;
  isInside(0,0);
  this->isIn = false;
}
void Button::update() {
  SDL_Rect textRect;
  SDL_QueryTexture(textTexture, NULL, NULL, &textRect.w, &textRect.h);
  srcRectText.x = 0;
  srcRectText.y = 0;
  srcRectText.h = textRect.h;
  srcRectText.w = textRect.w;
  destRectText.x = destRectObj.x + ((destRectObj.w - textRect.w)/2) ;
  destRectText.y = destRectObj.y + ((destRectObj.h - textRect.h)/2);
  destRectText.h = textRect.h;
  destRectText.w = textRect.w;
}

void Button::update(std::string text) {
  char const* ctext = text.c_str();
  textTexture = TextureManager::createTextureFromTextLight(ctext, this->textColor);
  SDL_Rect textRect;
  SDL_QueryTexture(textTexture, NULL, NULL, &textRect.w, &textRect.h);
  srcRectText.x = 0;
  srcRectText.y = 0;
  srcRectText.h = textRect.h;
  srcRectText.w = textRect.w;
  destRectText.x = destRectObj.x + ((destRectObj.w - textRect.w)/2) ;
  destRectText.y = destRectObj.y + ((destRectObj.h - textRect.h)/2);
  destRectText.h = textRect.h;
  destRectText.w = textRect.w;
}

bool Button::isInside(int x, int y){
  return x > destRectObj.x && x < (destRectObj.w + destRectObj.x) && y > destRectObj.y && y < (destRectObj.y + destRectObj.h);
}

bool Button::isInsideCircular(int x, int y){
  int cx = destRectObj.x + (destRectObj.w / 2);
  int cy = destRectObj.y + (destRectObj.h / 2);
  return sqrt(((cx-x)*(cx-x))+((cy-y)*(cy-y))) < ((destRectObj.w / 2)) && sqrt(((cx-x)*(cx-x))+((cy-y)*(cy-y))) < (destRectObj.h / 2);
}

void Button::highlight(bool hg){
  this->isIn = hg;
}

void Button::render(){
  if (this->isIn) {
    SDL_SetTextureColorMod(this->objTexture, 255,255,255);
  }else{
    SDL_SetTextureColorMod(this->objTexture, 180,180,180);
  }
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
  TextureManager::draw(this->textTexture, srcRectText, destRectText);
}

Button::~Button(){
  SDL_DestroyTexture(this->objTexture);
  SDL_DestroyTexture(this->textTexture);
}
