#ifndef _H_TILE_
#define _H_TILE_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "../objects/effect.hpp"

namespace graphics{
  class Tile{
  public:
    Tile(const char* bg,const char* flags,int tn, gopo::Effect*);
    Tile(const char* bg,SDL_Rect src, SDL_Rect dst);
    void addNumber();
    void update(SDL_Rect);
    void updateLastTile(SDL_Rect);
    void renderLastTile();
    void render();
    SDL_Texture* getTexture();
    std::string toString();
    gopo::Effect* getEffect();
    bool isInsideFlag(int x, int y);
    ~Tile();
  private:
    bool isInFlag = false;
    SDL_Texture* objTexture;
    gopo::Effect* eff;
    int tileNumber;
    int xpos, ypos;
    SDL_Texture* numberTexture;
    SDL_Texture* flagsTexture;
    SDL_Texture* effectTexture;
    SDL_Rect srcRectObj,srcRectNum,srcRectEffect, srcRectFlag;
    SDL_Rect drawDestRectObj,destRectObj,destRectNum,destRectEffect, destRectFlag;
  };
}

#endif
