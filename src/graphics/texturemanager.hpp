#ifndef _H_TEXTUREMANAGER_
#define _H_TEXTUREMANAGER_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

namespace graphics{
  class TextureManager{
  public:
    static SDL_Texture* loadTexture(char const* filename);
    static SDL_Texture* createTextureFromText(const char* text, const char* font, int size, SDL_Color color);
    static SDL_Texture* createTextureFromTextBold(const char* text, SDL_Color color);
    static SDL_Texture* createTextureFromTextLight(const char* text, SDL_Color color);
    static void draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);
  };
}

#endif
