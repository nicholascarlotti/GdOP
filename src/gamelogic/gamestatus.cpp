#include "./gamestatus.hpp"
#include <stdlib.h>
#include <string>
#include <vector>
#include "../objects/deck.hpp"
#include "../objects/map.hpp"
#include "../objects/player.hpp"
#include "./exceptions.hpp"
#include <stdlib.h>
#include <time.h>
#include <algorithm>

using namespace gopgl;
using namespace gopo;
using namespace std;

GameStatus::GameStatus(int playerCount, string playerNames[], int mapLength) : players(playerCount) {
    for (int i = 0; i < playerCount; i++) {
        this->players.at(i).setName(playerNames[i]);
    }
    random_shuffle(this->players.begin(), this->players.end());
    this->setProportionalCellCount(mapLength);
    this->upperMap = new Map(this->mapLength);
    this->lowerMap = new Map(this->mapLength);
    this->deck = new Deck(40);
    this->currPlayerIndex = -1;
    this->turn = -1;
    this->playerCount = playerCount;
}

GameStatus::~GameStatus() {
    this->players.clear();
    delete this->upperMap;
    delete this->lowerMap;
    delete this->deck;
}

Player* GameStatus::getNextPlayer() {
    int nextPlayerIndex = (this->currPlayerIndex + 1) % this->playerCount;
    return &this->players.at(nextPlayerIndex);
}

Player* GameStatus::getCurrentPlayer() {
    Player* result = NULL;
    if (this->currPlayerIndex != -1) {
        result = &this->players.at(this->currPlayerIndex);
    }
    return result;
}

Map* GameStatus::getUpperMap() { return this->upperMap; }

Map* GameStatus::getLowerMap() { return this->lowerMap; }

void GameStatus::setProportionalCellCount(int s){
  srand(time(NULL));
  switch (s) {
    case 1:
      this->mapLength = rand() % 40 + 40;
      break;
    case 2:
      this->mapLength = rand() % 60 + 80;
      break;
    case 3:
      this->mapLength = rand() % 110 + 140;
      break;
    case 4:
      this->mapLength = rand() % 150 + 250;
      break;
    default:
      this->mapLength = rand() % 40 + 40;
      break;
  }
}

int GameStatus::getCellCount() { return this->upperMap->getSize(); }

Deck* GameStatus::getDeck() { return this->deck; }

Player* GameStatus::getFirstPlayer() {
    Player* result = NULL;
    result = &this->players.at(0);
    for (int i = 1; i < this->playerCount; i++) {
        Player* p = &this->players.at(i);
        if (result->getPosition() < p->getPosition()) {
            result = p;
        }
    }
    return result;
}

Player* GameStatus::getLastPlayer() {
    Player* result = NULL;
    result = &this->players.at(0);
    for (int i = 1; i < this->playerCount; i++) {
        Player* p = &this->players.at(i);
        if (result->getPosition() > p->getPosition()) {
            result = p;
        }
    }
    return result;
}

int GameStatus::getTurn() { return this->turn; }

void GameStatus::newTurn() {
    this->turn++;
    this->currPlayerIndex = (this->currPlayerIndex + 1) % this->playerCount;
}

int GameStatus::getPlayerCount() { return this->playerCount; }

std::vector<Player>* GameStatus::getPlayers() { return &this->players; }

int GameStatus::getCurrentPlayerIndex(){
    return this->currPlayerIndex;
}

int GameStatus::getPlayerIndex(Player * p){
    int result = -1;
    if(p != NULL){
        bool found = false;
        for(int i = 0; i < this->playerCount && !found; i++){
            if(p == &this->players.at(i)){
                result = i;
                found = true;
            }
        }
    }
    return result;
}
