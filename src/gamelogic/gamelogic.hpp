#ifndef _H_GAMELOGIC_
#define _H_GAMELOGIC_

#include <string.h>
#include "gamestatus.hpp"
#include "../graphics/gameview.hpp"
#include "../objects/player.hpp"
#include "../graphics/gameoptions.hpp"

namespace gopo {
class Player;
class Effect;
}
namespace gopgl {

class DiceRollHandler{
    private:
        GameStatus& gameStatus;
        int * lastRoll;
    public:
        int handleRollChoice(int r1, int r2);
        /*
            Will move the current player first effect
            cell after his/her position + roll.
            If there is no next effect (e.g. end of the map)
            the player will stay at position + roll.

            Returns the final position of the current player.

        */
        int handleMoveToEffect(int roll);
        /*
            Moves the current player of max(r1, r2) - min(r1, r2)
            cells.

            Returns the final position of the current player.
        */
        int handleSubtractRoll(int r1, int r2);

        /*
            Moves the current player backwards.
            Returns the final position of the current player.
        */
        int handleMoveBackwards(int roll);

        /*
            If the roll is even, the current player
            will lose the turn. He/her moves forward
            otherwise.

            Return the final position of the player.
        */
        int handleEvenIsLostTurn(int roll);

        DiceRollHandler(GameStatus * gs) : gameStatus(*gs), lastRoll(NULL){}

        /*
            Rolls the dice for the current player and handles
            any effect from the 'effect die'.
            If 'simple_roll' is true, no effect will activate.

            Returns the final position of the current player or the result
            of a roll if 'simple_roll' is true.
        */
        int* roll(bool simple_roll = false);

        int* getLastRoll();
};

class EffectHandler {
   private:
    void handleDraw();
    void handleThrowBack();
    void handleRethrow();
    void handleSwitchWorld();
    void handleGoForward();
    void handleEnemiesBack();
    void handleEnemyLosesTurn();
    void handleSwitchFirst();
    void handleGoBack();
    void handleEnemyForward();
    void handleSwitchLast();
    void handleLoseTurn();
    void jump(gopo::Player* player, int cellCount, bool back = false);
    GameStatus& gameStatus;
    DiceRollHandler* diceRollHandler;

   public:
    /*
        Creates an EffectHandler. The EffectHandler highly interacts
        with the GameStatus, modifying it whenever an effect has to be
        handled.
    */
    EffectHandler(GameStatus* gs, DiceRollHandler* drh) : gameStatus(*gs), diceRollHandler(drh) {}
    void handleEffect(const gopo::Effect& e);
};

class GameLogic {
   private:
        graphics::GameView* gameview;
        gopgl::GameStatus* gameStatus;
        void statusCheck(gopo::Player& player);
        void checkCellEffect(gopo::Cell* playersCell);
        bool runRollPhase();
        bool runCardDrawPhase();
        EffectHandler* effectHandler;
        // void initializeMapCells();
        DiceRollHandler * diceRollHandler;

   public:
    /*
        Initializes all game's components (GameStatus, effect handlers, graphics)
        and handles the game flow.
    */
    GameLogic(graphics::GameOptions gameOptions);
    GameLogic(int playerCount, std::string* playerNames, int mapLenght);
    GameLogic(gopo::Player players [], int playerCount);

    int handleDiceChoice(int* dice);
    
    ~GameLogic();
    void start();
};
}
#endif
