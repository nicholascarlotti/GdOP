#ifndef _H_GAMESTATUS_
#define _H_GAMESTATUS_

#include <string>
#include <vector>

namespace gopo {
class Player;
class Cell;
class Deck;
class Map;
class Card;
}

namespace gopgl {

class GameStatus {
   public:
    GameStatus(int playerCount, std::string playerNames[], int mapLength);
    ~GameStatus();
    /*
        Gets next player in the succession,
        effectively starting his new turn.
    */
    gopo::Player* getNextPlayer();
    /*
        Gets the current player.
    */
    gopo::Player* getCurrentPlayer();
    int getCurrentPlayerIndex();

    /*
        Gets the players who is the furthest in game
        advancement.
    */
    gopo::Player* getFirstPlayer();
    /*
        Returns the player who has advanced the shortest
        in this game.
    */
    gopo::Player* getLastPlayer();
    /*
        Returns this game's upper map.
    */
    gopo::Map* getUpperMap();
    /*
        Returns this game's lower map.
    */
    gopo::Map* getLowerMap();
    /*
        Returns this game's deck.
    */
    gopo::Deck* getDeck();
    /*
        Returns the length of the map.
        Same as getMap()->getCells().size();
    */
    void setProportionalCellCount(int s);
    int getCellCount();
    /*
        Returns the game's current turn index.
        A new turn shall start whenever the next player
        in the succession makes his/her move.
    */
    int getTurn();
    /*
        Increments the turn counter.
    */
    void newTurn();
    /*
        Returns the number of players in this game.
    */
    int getPlayerCount();

    std::vector<gopo::Player>* getPlayers();
    int getPlayerIndex(gopo::Player * p);
   private:
    int turn;
    int playerCount;
    int mapLength;
    std::vector<gopo::Player> players;
    gopo::Map* upperMap;
    gopo::Map* lowerMap;
    gopo::Deck* deck;
    int currPlayerIndex;
};
}
#endif
