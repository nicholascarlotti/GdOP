#ifndef _H_DECK_
#define _H_DECK_
#include <vector>
#include "./card.hpp"
#include <stdexcept>

namespace gopo {
	class Deck {
	private:
		std::vector<gopo::Card> cards;
		void initializeDeck();
		int size;
	public:
		// Creates a deck of size equal to 
		// 'size' and initializes it.
		Deck(int size);
		gopo::Card draw();
		Card &getCard(int i) throw(std::out_of_range);
		Card *peek();
		std::vector<gopo::Card> * getCards();
		//Returns the number of cards currently
		//sitting in the deck.
		int getCardsCount();
	};
}
#endif
