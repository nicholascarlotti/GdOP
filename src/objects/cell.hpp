#ifndef _H_CELL_
#define _H_CELL_
#include "./effect.hpp"

namespace gopo {
	class Cell {
	private:
		gopo::Effect effect;

	public:
		Cell();
		Cell(gopo::Effect);
		void setEffect(gopo::Effect);
		gopo::Effect *getEffect();
		~Cell();
	};
}
#endif
