#include "deck.hpp"
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include "card.hpp"
#include "effect.hpp"
#include "ctime"
using namespace gopo;
using namespace std;

Deck::Deck(int size) : cards(0, Card(Effect(Action::empty), Effect(Action::empty))) {
    this->size = size;
    this->initializeDeck();
}

Card Deck::draw() {
    if (this->cards.size() <= 0) {
        this->initializeDeck();
    }
    Card result = this->cards.back();
    this->cards.pop_back();
    return result;
}

Card* Deck::peek(){
    Card * result = NULL;
    if(this->cards.empty()){
        this->initializeDeck();
    }
    result = &this->cards.back();
    return result;
}

vector<Card> * Deck::getCards() { return &this->cards; }

Card& Deck::getCard(int i) throw(std::out_of_range) {
    return this->cards.at(i);
}

void Deck::initializeDeck() {
    int probCeil = 0;
    int effectIndex1 = Action::goForward;
    int effectIndex2 = Action::goForward;
    srand(time(0));
    while (effectIndex1 < Action::LAST_ACTION) {
        probCeil += EFFECTS_PROBABILITY_DISTR.at((Action)effectIndex1);
        effectIndex1++;
    }
    for (int i = 0; i < this->size; i++) {
        effectIndex1 = Action::END_OF_CELL_EFFECTS;
        effectIndex2 = Action::END_OF_CELL_EFFECTS;
        
        int randInt = (rand() % probCeil) + 1;
        while (effectIndex1 < Action::LAST_ACTION && randInt > 0) {
            effectIndex1++;
            randInt -= EFFECTS_PROBABILITY_DISTR.at((Action)effectIndex1);
        }
            randInt = (rand() % probCeil) + 1;
            while (effectIndex2 < Action::LAST_ACTION && randInt > 0) {
                effectIndex2++;
                randInt -= EFFECTS_PROBABILITY_DISTR.at((Action)effectIndex2);
            }   

        Card newCard = Card(Effect((Action)effectIndex1), Effect((Action)effectIndex2));
        this->cards.push_back(newCard);
    }
}

int Deck::getCardsCount(){
    return this->cards.size();
}
