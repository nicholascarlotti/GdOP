// #include "../gamelogic/gamestatus.hpp"
#include "./effect.hpp"
#include "./cell.hpp"

using namespace gopo;

Cell::Cell() {}

Cell::Cell(Effect effect) {
  this->setEffect(effect);
}

void Cell::setEffect(Effect effect) {
  this->effect = effect;
}

Effect* Cell::getEffect() {
  return &this->effect;
}

Cell::~Cell() {}
