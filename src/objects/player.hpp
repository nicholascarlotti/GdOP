#ifndef _H_PLAYER_
#define _H_PLAYER_

#include <string>

namespace gopo {
	class Player {
	private:
		std::string name;
		int position;
		bool inLowerMap;
		bool loseNextTurn;
	public:
		Player();
		Player(std::string & name);
		Player(std::string name);
		int move(int steps);
		int getPosition();
		bool isInLowerMap();
		void removeStatus();
		void setName(const std::string &name);
		void setPosition(int position);
		void setInLowerMap(bool inLowerMap);
		std::string getName();
		bool setLoseNextTurn(bool b);
		bool losesNextTurn();
		~Player();
	};
}

#endif
