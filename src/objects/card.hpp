#ifndef _H_CARD_
#define _H_CARD_

#include "effect.hpp"
#include <string>

namespace gopo {
	class Card {
	private:
		gopo::Effect effect1,effect2;
	public:
		Card(gopo::Effect, gopo::Effect);
		void setEffect1(gopo::Effect);
		void setEffect2(gopo::Effect);
		gopo::Effect getEffect1();
		gopo::Effect getEffect2();
		~Card();
	};
}
#endif
