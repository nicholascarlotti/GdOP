#define DEBUG = true

#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "gamelogic/logger.hpp"
#include "gamelogic/gamelogic.hpp"
#include "graphics/gameoptions.hpp"
#include <iostream>

int main(int argc, char const *argv[])
{
    //gopgl::log_message("Gioco dell'Oca Pazza");
    //gopgl::log_message("Quanti giocatori?");
    //int players = 8;
    // std::cin >> players;
    //gopgl::log_message("Insert player names");
    // std::string * names = new std::string[players];
    // for(int i = 0; i < players; i++){
    //     std::cin >> names[i];
    // }
    graphics::GameOptions opt = graphics::GameOptions();
    opt.setNumberOfPlayers();
    //std::cout << opt.getNumberOfPlayers() << '\n';
    opt.setPlayerNames(opt.getNumberOfPlayers());
    opt.setMapLength();
    //std::string names[] = {"Marek", "Nico","Poppo","Sam", "Santo", "Luca", "Marco", "Frenk"};
    gopgl::GameLogic game = gopgl::GameLogic(opt);
    game.start();


    //delete names;
    //names = NULL;
    return 0;
}
