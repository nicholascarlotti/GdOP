#include "gtest/gtest.h"

namespace {
class GameStatusTest : public ::testing::Test {
 public:
  GameStatus *status;

  virtual void SetUp() {
    std::string arr[3] = {"pippo", "pluto", "topolino"};
    this->status = new GameStatus(3, arr);
    this->status->newTurn();

    // As a consequence of this loop, players' positions should
    // be: pippo: 3    pluto: 6   topolino: 9
    for (int i = 0; i < status->getPlayerCount(); i++) {
      status->getPlayers()->at(i).setPosition(3 * (1 + i));
    }
  }

  virtual void TearDown() { delete this->status; }
};

TEST_F(GameStatusTest, GetNextPlayer) {
  Player *next = status->getNextPlayer();
  ASSERT_STREQ(next->getName().c_str(), "pluto");
}

TEST_F(GameStatusTest, GetLastPositionedPlayer) {
  Player *next = status->getLastPlayer();
  ASSERT_STREQ(next->getName().c_str(), "pippo");
  next->setPosition(20);
  next = status->getLastPlayer();
  ASSERT_STRNE(next->getName().c_str(), "pippo");
  ASSERT_STREQ(next->getName().c_str(), "pluto");
}

TEST_F(GameStatusTest, GetFirstPositionedPlayer){
  Player *next = status->getFirstPlayer();
  ASSERT_STREQ(next->getName().c_str(), "topolino");

}

TEST_F(GameStatusTest, GetPlayerIndex){
    Player * next = status->getNextPlayer();
    ASSERT_EQ(1, status->getPlayerIndex(next));
}

TEST_F(GameStatusTest, MapsSize) {
  ASSERT_EQ(status->getUpperMap()->getSize(), status->getLowerMap()->getSize());
}
}