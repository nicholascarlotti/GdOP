#include "../src/objects/deck.hpp"

namespace {
	class DeckTest : public ::testing::Test{
	public:
		Deck * deck;
		virtual void SetUp(){
			this->deck = new Deck(10);
		}

		virtual void TearDown(){
			delete this->deck;
		}

	};

	TEST_F(DeckTest, TestDeckReshuffle){
		ASSERT_EQ(10, deck->getCardsCount());
		for(int i = 0; i < 10; i++){
			deck->draw();
		}
		ASSERT_EQ(0, deck->getCardsCount());
		deck->draw();
		ASSERT_EQ(9, deck->getCardsCount());

	}
}