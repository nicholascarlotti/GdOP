#include "../src/gamelogic/gamelogic.hpp"
#include "../src/gamelogic/gamestatus.hpp"
#include "../src/objects/player.hpp"
#include "../src/objects/map.hpp"

namespace {
	using namespace gopo;
	using namespace gopgl;

	class DiceRollTest : public ::testing::Test{
		public:
			DiceRollHandler * dice;
			GameStatus * status;

		virtual void SetUp(){
	        std::string arr[3] = {"pippo", "pluto", "topolino"};
			this->status = new GameStatus(3, arr, 40);
			this->dice = new DiceRollHandler(this->status);
	        this->status->newTurn();

		}

		virtual void TearDown(){
			delete this->dice;
			delete this->status;
		}
	};

	TEST_F(DiceRollTest, MoveToEffectStandard){
		// [p] [ ] [ ] [ ] [ ] [e]
		Map * map = this->status->getUpperMap();
		for(int i = 10; i < 15; i++){
			map->getCell(i)->setEffect(Action::empty); // should not work

		}
		map->getCell(15)->setEffect(Action::drawOne);
		Player * p = this->status->getCurrentPlayer();
		p->setPosition(10);
		dice->handleMoveToEffect(1 + 2);
		ASSERT_EQ(15, p->getPosition());
	}

	TEST_F(DiceRollTest, MoveToEffectToWin){
		// [p] [ ] [ ] [ ] [ ] [e]
		Map * map = this->status->getUpperMap();
		for(int i = 35; i < 40; i++){
			map->getCell(i)->setEffect(Action::empty); // should not work

		}
		Player * p = this->status->getCurrentPlayer();
		p->setPosition(35);
		dice->handleMoveToEffect(6 + 6);
		ASSERT_EQ(47, p->getPosition());
	}

	TEST_F(DiceRollTest, HandleSubtractRollTest){
		Player * p = this->status->getCurrentPlayer();
		dice->handleSubtractRoll(4, 2);
		ASSERT_EQ(2, p->getPosition());
		dice->handleSubtractRoll(1, 5);
		ASSERT_EQ(6, p->getPosition());

	}

	TEST_F(DiceRollTest, HandleMoveBackwards){
		Player * p = this->status->getCurrentPlayer();
		p->setPosition(13);
		int result = dice->handleMoveBackwards(5);
		ASSERT_EQ(8, p->getPosition());
		ASSERT_EQ(8, result);
	}

	TEST_F(DiceRollTest, HandleEvenIsLostTurn){
		Player * p = this->status->getCurrentPlayer();
		p->setPosition(15);
		int result = dice->handleEvenIsLostTurn(4);
		ASSERT_EQ(15, result);
		ASSERT_EQ(15, p->getPosition());
		result = dice->handleEvenIsLostTurn(3);
		ASSERT_EQ(18, result);
		ASSERT_EQ(18, p->getPosition());
	}
}