#include "../src/objects/player.hpp"
#include "gtest/gtest.h"

namespace {
using namespace gopo;

class PlayerTest : public ::testing::Test {
   public:
    Player player;

    virtual void SetUp() { this->player = Player("pippo"); }
};

TEST(PlayerInitialization, DefaultConstructor) {
    Player p = Player();
    ASSERT_STREQ("", p.getName().c_str());
}

TEST(PlayerInitialization, ParametricConstructor) {
    Player p = Player("pippo");
    ASSERT_STREQ("pippo", p.getName().c_str());
}

TEST_F(PlayerTest, StandardMovement) {
    ASSERT_EQ(0, player.getPosition());
    player.setPosition(5);
    ASSERT_EQ(05, player.getPosition());
    player.move(10);
    ASSERT_EQ(15, player.getPosition());
    player.move(-7);
    ASSERT_EQ(8, player.getPosition());
}
}