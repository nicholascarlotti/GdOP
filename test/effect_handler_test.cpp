#include "../src/objects/effect.hpp"
#include <string>
#include "../src/gamelogic/gamelogic.hpp"
#include "../src/gamelogic/gamestatus.hpp"
#include "../src/objects/card.hpp"
#include "../src/objects/deck.hpp"
#include "../src/objects/effect.hpp"
#include "gtest/gtest.h"

namespace {
using namespace gopgl;
using namespace gopo;
class EffectHandlerTest : public ::testing::Test {
   public:
    EffectHandler *effectHandler;
    GameStatus *status;
    DiceRollHandler * diceRollHandler;
    virtual void SetUp() {
        std::string arr[3] = {"pippo", "pluto", "topolino"};
        this->status = new GameStatus(3, arr);
        this->diceRollHandler = new DiceRollHandler(this->status);
        this->effectHandler = new EffectHandler(this->status, this->diceRollHandler);
        this->status->newTurn();
    }

    virtual void TearDown() {
        delete this->effectHandler;
        delete this->status;
        delete this->diceRollHandler;
    }
};

TEST_F(EffectHandlerTest, DrawOne) {
    Effect e = Effect(drawOne);
    int currentDeckSize = status->getDeck()->getCards()->size();
    effectHandler->handleEffect(e);
    ASSERT_NE(currentDeckSize, status->getDeck()->getCards()->size());
}

TEST_F(EffectHandlerTest, ThrowAndGoBack) {
    Player *p = status->getCurrentPlayer();
    p->move(20);
    Effect e = Effect(Action::throwAndGoBack);
    effectHandler->handleEffect(e);
    ASSERT_GT(20, p->getPosition());
}

TEST_F(EffectHandlerTest, Rethrow) {
    Player *p = status->getCurrentPlayer();
    p->move(20);
    Effect e = Effect(Action::rethrow);
    effectHandler->handleEffect(e);
    ASSERT_LT(20, p->getPosition());
}

TEST_F(EffectHandlerTest, GoBack) {
    Player *p = status->getCurrentPlayer();
    Effect e = Effect(Action::goForward);
    effectHandler->handleEffect(e);
    ASSERT_LT(0, p->getPosition());
}
TEST_F(EffectHandlerTest, EnemiesBack) {
    for(int i = 0; i < status->getPlayerCount(); i++){
    	status->getPlayers()->at(i).setPosition(10);
    }
    Effect e = Effect(Action::enemiesBack);
    effectHandler->handleEffect(e);
    Player *curr = status->getCurrentPlayer();
    ASSERT_EQ(10, curr->getPosition());

    for(int i = 1; i < status->getPlayerCount(); i++){
    	ASSERT_GT(10, status->getPlayers()->at(i).getPosition());
    }
}

TEST_F(EffectHandlerTest, NextEnemyForward) {
    for(int i = 0; i < status->getPlayerCount(); i++){
    	status->getPlayers()->at(i).setPosition(10);
    }
    Effect e = Effect(Action::enemyForward);
    effectHandler->handleEffect(e);
    Player *curr = status->getCurrentPlayer();
    ASSERT_EQ(10, curr->getPosition());
    Player *next = status->getNextPlayer();
    ASSERT_LT(curr->getPosition(), next->getPosition());
    
}

TEST_F(EffectHandlerTest, SwitchLast){
	for(int i = 0; i < status->getPlayerCount(); i++){
    	status->getPlayers()->at(i).setPosition(3 * (1 + i));
    }
    //As a consequence of this loop, players' positions should
    //be: [0]: 3	[1]: 6   [2]: 9

    Player * curr = status->getCurrentPlayer();
    curr->setPosition(20);
    Effect e = Effect(Action::switchLast);
    effectHandler->handleEffect(e);
    EXPECT_GT(20, curr->getPosition());
    ASSERT_EQ(6, curr->getPosition());
}


TEST_F(EffectHandlerTest, SwitchFirst){
	for(int i = 0; i < status->getPlayerCount(); i++){
    	status->getPlayers()->at(i).setPosition(3 * (1 + i));
    }
    //As a consequence of this loop, players' positions should
    //be: [0]: 3	[1]: 6   [2]: 9

    Player * curr = status->getCurrentPlayer();
    Player * firstPlayer = status->getFirstPlayer();
    curr->setPosition(0);
    Effect e = Effect(Action::switchFirst);
    effectHandler->handleEffect(e);
  	EXPECT_FALSE(curr == firstPlayer);
    EXPECT_LT(0, curr->getPosition());
    ASSERT_EQ(9, curr->getPosition());
    ASSERT_EQ(0, firstPlayer->getPosition());
}

TEST_F(EffectHandlerTest, LoseTurn){
    Player * curr = status->getCurrentPlayer();
	Effect e = Effect(Action::loseTurn);
	effectHandler->handleEffect(e);
	for(int i = 0; i < 3; i++){
		status->newTurn();
		if(curr->getStatus()->check()){
			curr->getStatus()->trigger();
			curr->removeStatus();
		}

	}
	ASSERT_NE(curr, status->getCurrentPlayer());
}

TEST_F(EffectHandlerTest, EnemyLoseTurn){
    Player * next = status->getNextPlayer();
	Effect e = Effect(Action::enemyLosesTurn);
	effectHandler->handleEffect(e);
	status->newTurn();
	if(next->getStatus()->check()){
		next->getStatus()->trigger();
		next->removeStatus();
	}
	ASSERT_NE(next, status->getCurrentPlayer());
}

}