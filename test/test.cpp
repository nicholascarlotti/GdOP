#include "gtest/gtest.h"
#include "map_test.cpp"
#include "player_test.cpp"
#include "effect_handler_test.cpp"
#include "gamestatus_test.cpp"
#include "dice_roll_handler.cpp"
#include "deck_test.cpp"

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}