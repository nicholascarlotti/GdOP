#include "../src/objects/map.hpp"
#include "gtest/gtest.h"

namespace {
using namespace gopo;

TEST(MapCreation, ConstructorCreation) {
    int map_size = 40;
    Map m = Map(map_size);
    ASSERT_EQ(map_size, m.getCells().size());
    ASSERT_EQ(map_size, m.getSize());
}

TEST(MapCreation, FactoryMethodCreation) {
    int map_size = 40;
    Map m = Map::createMap(map_size);
    ASSERT_EQ(map_size, m.getCells().size());
    ASSERT_EQ(map_size, m.getSize());
}
}